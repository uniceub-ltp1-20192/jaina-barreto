from Entidades.automovel import Automovel

class Carro(Automovel):

    def __init__(self, portas=0):
        super().__init__()
        self._portas = portas
    
    @property
    def portas(self):
        return self._portas
    
    @portas.setter
    def portas(self,portas):
        self._portas = portas

    def __str__(self):
        return """ Dados de Automóvel:
        Identificador: {}
        montadora = {}
        modelo = {}
        ano = {}
        cor = {}
        portas = {}
        """.format(self.identificador,
        self.identificador,
        self.montadora,
        self.modelo,
        self.ano,
        self.cor,
        self.portas)

    def estacionar(self):
        print("Plic")