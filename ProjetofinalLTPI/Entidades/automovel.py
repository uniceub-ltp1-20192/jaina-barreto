class Automovel:
  
  def __init__(self, identificador = 0, montadora="", modelo="", ano=0, cor=""):
    self._identificador = identificador
    self._montadora = montadora
    self._modelo = modelo
    self._ano = ano
    self._cor = cor

  @property
  def identificador(self):
    return self._identificador

  @identificador.setter
  def identificador(self,identificador):
    self._identificador = identificador

  @property
  def montadora(self):
    return self._montadora

  @montadora.setter
  def montadora(self,montadora):
    self._montadora = montadora

  @property
  def modelo(self):
    return self._modelo

  @modelo.setter
  def modelo(self,modelo):
    self._modelo = modelo

  @property
  def ano(self):
    return self._ano

  @ano.setter
  def ano(self,ano):
    self._ano = ano

  @property
  def cor(self):
    return self._cor

  @cor.setter
  def cor(self,cor):
    self._cor = cor

  def acelerar(self):
    print("Vrum")

  def estacionar(self):
    print("Estacionado")
