def valor(msg):
    while True:
        try:
            x = float(input(msg))
            if x < 0:
                raise NameError
            break
        except ValueError:
            print("Digite somente números.\n")
        except NameError:
                print("Digite um número positivo1.\n")
    return x

def quantidade(msg):
    while True:
        try:
            x = int(input(msg))
            if x < 0:
                raise NameError
            break
        except ValueError:
            print("Digite somente números inteiros.\n")
        except NameError:
                print("Digite um número positivo.\n")
    return x

def resultados():
    a, b = 0, 0
    while True:
        try:
            x = int(input("Digite 1 para adicionar um produto ou 2 para sair do programa:"))
            if x == 2:
                break
            if x != 1:
                raise TypeError

            y = valor("Qual o valor do produto:")
            z = quantidade("Quantidade do produto:")
            w = valor("Qual o percentual de desconto(0-100):")
            a += y*z
            b += (y*z)*(1-(w/100))

        except ValueError:
            print("Digite somente números.\n")
        except TypeError:
            print("Digite somente 1 ou 2.\n")

    return a, b

x, y = resultados()
print("Este é o valor sem desconto:", x, "\n Este é o valor com desconto:", y)
