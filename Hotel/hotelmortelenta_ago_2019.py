#No hotel morte lenta, estavam hospedados 5 hospédes, Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
#A meia noite, o hóspede do quarto 101 foi encontrado morto e os 5 suspeitos são os citados acima.
#Descubra quem é o assassino sabendo que:
#        Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é u ou o, ele pode ser o assassino.
#        Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
#        Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.
# Sabendo dessas informações acima, projete o algorítmo que fornecido o nome do suspeito, indique se ele pode ou não ser o assassino.

import sys
import re
import random

#biogender of each hosped
M = ('Renato Faca', 'Roberto Arma', 'Uesllei')
F = ('Maria Cálice', 'Roberta Corda')
people = ('Renato Faca', 'Roberto Arma', 'Uesllei', 'Maria Cálice', 'Roberta Corda')

#real weapons
weapon = ('arma', 'metralhadora', 'bomba')

#defining random time for people who were at the lobby
minutes = {0:59}
hours = {0:23}
h = random.choice(hours) 
m = random.choice(minutes)

def askName():
	return input(str('Type the name of whom you want to know if is a suspect: ').lower())

def validateString(name):
        if  all(x.isalpha() or x.isspace() for x in name):
                return True

def validateVowels(name):
        count_vowels = 0 
        for v in name.lower():
            if v in 'aei':
                count_vowels += 1
        if count_vowels >= 3 and ('o' not in name or 'u' not in name):
                return True

def getBiogender(name):
        if name in M:
            gender = 'M'
        elif name in F:
            gender = 'F'
        elif name not in M and name not in F:
            gender = input("What's his/her gender (upcase M/F)? ")

def checkWhiteWeapon():
        if gender == 'F':
                for s in weapon:
                        if name in s:
                                return True

def lobbyTime():
        if gender == 'M':
                h == 0 and m == 30


#-------------------------FLUXO PRINCIPAL-------------------------

# ask a name to the user
name = askName()

# validade the name as a string
vs = validateString(name)

if not vs:
	print('Please type a valid name and familyname (only letters)')
	name = askName()

# business rule 1 (vowels)
vv = validateVowels(name)

if not vv:
	sys.exit('Innocent')

# verify or ask gender
gender = getBiogender(name)

# business rule 2 (woman with white weapon)
weapon = checkWhiteWeapon()
	
if not weapon:
        sys.exit('Innocent')

# business rule 3 (man at the lobby at 00:30)
time = lobbyTime()
print ('Time at the lobby', h, ':', m)

if not time:
        sys.exit('Innocent')

print("Suspect")
