/*No hotel morte lenta, estavam hospedados 5 hospédes, Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
A meia noite, o hóspede do quarto 101 foi encontrado morto e os 5 suspeitos são os citados acima.
Descubra quem é o assassino sabendo que:
        Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é u ou o, ele pode ser o assassino.
        Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
        Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.
 Sabendo dessas informações acima, projete o algorítmo que fornecido o nome do suspeito, indique se ele pode ou não ser o assassino.*/

#include <iostream>
#include <string>
#include <regex>

using namespace std;

string name;
regex vowels ("(aeiou[^ ]+)?)");
int count_vowels;

void askName()
{
    cout << "Type the name of whom you want to know if he/she is a suspect: ";
    cin >> name;
}

void checkString (string name)
{
    for(int i = 0; i < name.size(); i++)
    {
        if (!isalpha(name[i])){
            cout << "Input must only contain letters and space\n";
            askName();
        }
    }
}
void checkVowels (string name, regex vowels)
{
    smatch matches;
    regex_search(name, matches, vowels);
        if(matches.size()  < 3)
            cout << "Innocent";
}

int main()
{
    
    askName();
    
    checkString(name);
    
    checkVowels(name, vowels);
    
    
    
    return 0;
}
