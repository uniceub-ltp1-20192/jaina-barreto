class cachorro: #ABSTRAÇAO
  def __init__(self, tamanho = 0.0, peso = 0.0, cor = ""):
    #são itens privados = ENCAPSULAMENTO
    self._tamanho = tamanho
    self._peso = peso
    self._cor = cor

  @property #anotation é um método de get
  def tamanho(self): #definindo um método para pegar o valor de tamanho
    return self._tamanho

  @tamanho.setter
  def tamanho(self, tamanho):
    self._tamanho = tamanho

  @property
  def peso(self):
    return self._peso

  @peso.setter
  def peso(self, peso):
    self._peso = peso

  @property
  def cor(self):
    return self._cor

  @cor.setter
  def cor(self, cor):
    self._cor = cor

  def latir(self):
    return "Au Au"

  def comer(self):
    print("args args")