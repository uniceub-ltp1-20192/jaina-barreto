from entidades.cachorro import cachorro

v = cachorro() #quando eu chamo cachorro dessa forma, ele vai lá no init e cria um ovo cachorro

v.tamanho = 3.4
v.peso = 3.9
v.cor = "Azul"

print(v)
print("""O tamanho do cachorro é {}
O peso do cachorro é {}
A cor do cachorro é {}
Ele faz {}""".format(v.tamanho,v.peso,v.cor,v.latir()))
print(v.latir())
print(vars(v))