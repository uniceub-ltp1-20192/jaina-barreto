#calculadora

def askNumber():
  while True:
    try:
      number = float(input('Digite um número: '))
      return number
    except:
      print('Digite apenas números.')

def askOperator():
  while True:
    try:
      operadores = ['+', '-','*','/']
      op = (input('Digite o operador(+-*/): '))
      for i in operadores:
        if op == i:
          return op
      raise ValueError
    except ValueError:
      print('Digite apenas um dos operadores: +-*/')

def calculator():
  value1 = askNumber()
  operator = askOperator()
  value2 = askNumber()

  if operator == '+':
    print('Resultado: ', value1 + value2)
  elif operator == '-':
    print('Resultado: ', value1 - value2)
  elif operator == '*':
    print('Resultado: ', value1 * value2)
  elif operator == '/':
    print('Resultado: ', value1 / value2)

calculator()