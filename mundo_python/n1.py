"""1.1 Escreva um programa que imprima na tela “Já é meu
segundo programa”
1.2 Escreva um programa que imprima na tela um menu com
três opções: 1 ­ cadastrar, 2 – logar e 3 – sair, cada
opção e uma linha.
1.3 Escreva um programa que imprima os números pares de 1
até 10."""

def n1():
	print("--------------N1.0--------------")
	print("Olá mundo!")

	# N1.2
	print("--------------N1.1--------------")
	print("Já é meu segundo programa")

	# N1.3
	print("--------------N1.2--------------")
	print("1 - cadastrar \n2 - logar \n3 - sair")

	# N1.4
	print("--------------N1.3--------------")
	fim = 10
	x = 0

	while x <= fim:
		if x % 2 == 0:
			print(x)
		x = x + 1

n1()