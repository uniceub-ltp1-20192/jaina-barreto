"""4.1 Armazene o nome de uma pessoa e inclua alguns
caracteres de espaço em branco no início e no final do
nome. Certifique­se de usar cada combinação de
caracteres, "\t" e "\n", pelo menos uma vez. Imprima o
nome uma vez, para que o espaço em branco ao redor do
nome seja exibido. Em seguida, imprima o nome usando cada
uma das três funções de DECAPAGEM (stripping) lstrip(),
rstrip() e strip().
4.2 Solicite uma frase ao usuário e apresente quantos
caracteres existem na frase sem contar os espaços em
branco."""


def n4():

  print("-------EXERCICIO 4.1-------")
  linguagem_favorita = ' python '
  um = linguagem_favorita.lstrip()
  dois = linguagem_favorita.rstrip()
  tres = linguagem_favorita.strip()
  print(linguagem_favorita)
  print(dois)
  print(tres)

  print("-------EXERCICIO 4.2-------")
  frase = input("Digite uma algo para as letras serem contadas: ")
  frase = frase.replace(" ","")
  #x = len(frase)
  print("Na frase há", len(frase) ,"caracteres")

n4()
