""""9.1 Implemente um código em Python que some todos elementos dentro de uma lista.
9.2 Implemente um código em Python que multiplique todos elementos dentro de uma lista.
9.3 Escreva um código que retorne o menor elemento de uma lista.
9.4 Escreva um código que fornecidas duas listas imprima uma lista com os elementos comuns entre elas.
Exemplo: ListaA = [“ABC”, “DEF”, “GHI”, “YWZ”]
ListaB = [“CEB”, “YWZ”, “ABC”]
Saída: [“ABC”, “YWZ”]
9.5 Implemente um programa que imprima todos elementos de uma lista em ordem alfabética
9.6 Implemente um código que imprima uma lista com o tamanho de cada elemento da lista de entrada.
Exemplo: lista = [“cebola”, “carro”, “oi”, “mansões”]
Saída: [6,5,2,7]
9.7 Implemente um programa que fornecida uma lista de palavras e uma palavra indique quais elementos da lista
são anagramas da palavra."""

def n9():

    print("-------EXERCICIO 9.1-------")
    valores = [6, 9, 1, 71, 8, 32, 3, 52]
    print('Lista inicial: ', valores)
    
    soma = 0
    for numero in valores:
        soma = soma + numero
    print('Soma dos itens:', soma)

    print("-------EXERCICIO 9.2-------")
    multi = 1
    for numero in valores:
        multi = multi * numero
    print('Multiplicação dos itens:', multi)
        
    print("-------EXERCICIO 9.3-------")
    valores.sort()    
    print('Menor valor da lista:', valores[0])

    print("-------EXERCICIO 9.4-------")
    listaA = ['ABC', 'DEF', 'GHI', 'YWZ']
    listaB = ['CEB', 'YWZ', 'ABC']
    print(list(set(listaA).intersection(listaB)))

    print("-------EXERCICIO 9.5-------")
    nomes = ['cebola', 'carro', 'oi', 'mansões']
    print('Lista inicial:', nomes)
    print('Lista em ordem alfabética:', sorted(nomes))

    print("-------EXERCICIO 9.6-------")
    qntd_letras = []
    for nome in nomes:
        qntd_letras.append(len(nome))
    print(qntd_letras)
    
    print("-------EXERCICIO 9.7-------")
    palavra = sorted('rac')
    alternativas = ['car', 'girl', 'tofu', 'rca']

    for alt in alternativas:
        if palavra == sorted(alt):
            print(alt)

n9()
