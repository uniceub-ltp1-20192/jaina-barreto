"""5.1 Crie um programa que solicite um valor ao usuário e
informe se o mesmo é par ou impar.
5.2 Conceba uma programa que informe se um determinado
número é primo.
5.3 Crie uma calculadora automática que dado dois valores
informe o resultado da adição, subtração, multiplicação e
divisão.
5.4 Crie uma programa que fornecidos a idade e o semestre
que um pessoa está informa em quantos anos ela vai se
formar.
5.5 Crie uma variação do programa 5.4 que receba também a
informação de quantos semestres o aluno está atrasado e
indique qual o tempo total que ele vai ter no curso até
se formar."""


def quesaco(msg):
 while True:
   try:
     valor = int(input(msg))
     break
   except ValueError:
     print("Digite somente números.\n")
 return int
     

def n5():
 print("-------EXERCICIO 5.1-------")
 while True:
   try:
     numero = int(input("Digite um número:"))
     break
   except ValueError:
     print("Digite somente números.\n")
 if numero % 2 == 0:
  print(numero, "é par")
 else:
  print(numero, "é ímpar")

 print("-------EXERCICIO 5.2-------")
 divisores = 0
 while divisores <= 3:
  for n in range(1, numero + 1):
    if numero % n == 0:
      divisores += 1
  break

 if divisores == 2:
   print("é primo")
 else:
   print("não é primo")
 
 print("-------EXERCICIO 5.3-------")
 while True:
   try:
     a = float(input("Digite o primeiro valor:"))
     b = float(input("Digite o segundo valor:"))
     break
   except ValueError:
     print("Digite somente números.\n")

 soma = a+b
 sub = a-b
 mult = a*b
 div = a/b
 print("Soma:", soma)
 print("Subtração:", sub)
 print("Multiplicação:", mult)
 print("Divisão:", div)
 
 print("-------EXERCICIO 5.4-------")
 semestre_atual = int(input("Está em que semestre?"))
 semestre_total = int(input("Quantos semestres tem seu curso?"))
 idade = int(input("Qual a sua idade?"))

 falta = semestre_total - semestre_atual

 idade = int(idade + (falta/2))

 print("A previsão é de que você vai se formar com", idade, "anos em", falta, "semestres")
 
   print("-------EXERCICIO 5.5-------")
  atraso = str(input("Você atrasou algum semestre? (S/N) "))  
  semestre_atual = int(input("Está em que semestre? "))
  semestre_total = int(input("Quantos semestres tem seu curso? "))
  idade = int(input("Qual a sua idade? "))

  if atraso == 'S':
    falta = semestre_atual - semestre_total

  idade = int(idade + (falta/2))

  print("A previsão é de que você vai se formar com", idade, "anos em", falta, "semestres")
 
 n5()