"""12.1 Um buffet oferece 5 tipos de comida. Pense em 5 tipos de comida e as guarde em um tupla.
• Use um loop for para imprimir cada comida que o restaurante oferece.
• Tente modificar um dos itens, e garanta que o python rejeita a alteração.
• O restaurante mudou o menu, trocando dois pratos do cardápio.
Adicione um bloco de código que rescreve a tupla, e então reusa o loop for para imprimir os itens do novo menu."""

def n12():

    print("-------EXERCICIO 12.1-------")
    menu_1 = ('suco', 'cerveja', 'batata frita', 'pizza')
    print('Primeiro menu:')
    for opcoes in menu_1:
        print(opcoes.title())

    """menu_1[0] = 'caipirinha'
Traceback (most recent call last):
  File "/Users/jainabarreto/Documents/CEUB/jaina-barreto/mundo_python/n12.py", line 26, in <module>
    n12()
  File "/Users/jainabarreto/Documents/CEUB/jaina-barreto/mundo_python/n12.py", line 15, in n12
    menu_1[0] = 'caipirinha'
TypeError: 'tuple' object does not support item assignment
    
    menu_1.append('pipoca')
Traceback (most recent call last):
  File "/Users/jainabarreto/Documents/CEUB/jaina-barreto/mundo_python/n12.py", line 18, in <module>
    n12()
  File "/Users/jainabarreto/Documents/CEUB/jaina-barreto/mundo_python/n12.py", line 16, in n12
    menu_1.append('pipoca')
AttributeError: 'tuple' object has no attribute 'append'"""

    menu_1 = ('suco', 'cerveja', 'moqueca', 'feijoada')
    print('\nSegundo menu:')
    for opcoes in menu_1:
        print(opcoes.title())

n12()
