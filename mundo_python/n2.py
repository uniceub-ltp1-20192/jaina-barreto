"""2.1 Crie um programa que defina duas variáveis, quantidade_cebolas
e preco_cebola, atribua valores inteiros para essas variáveis.
Imprima o valor de quantidade_cebolas muliplicado pelo
preco_cebola, da seguinte forma:
“Você irá pagar” + resultado da multiplicação + “pelas cebolas.”

2.2 Faça um comentário em português em cada linha do programa
abaixo explicando para você mesmo que elas fazem. Execute o
programa abaixo, corrigindo os erros.

2.3 Brinque com os valores das variáveis do programa 2.2 e veja o
que acontece com a saída do seu programa."""


def quantidade(msg):
    while True:
        try:
            x = int(input(msg))
            if x < 0:
                raise NameError
            break
        except ValueError:
            print("Digite somente números inteiros.\n")
        except NameError:
                print("Digite um número positivo.\n")
    return x


def n2():
	print("--------------N2.1--------------")
	quantidade_cebolas = 2
	preco_cebola = 1
	valor_total = quantidade_cebolas*preco_cebola
	print("Você irá pagar R$", valor_total, "pelas cebolas.")

	print("--------------N2.2--------------")
	cebolas = 300 #definindo a variavel cebolas (unidades de cebolas)
	cebolas_na_caixa = 120 #definindo a variavel cebolas_na_caixa (unidades dentro da caixa)
	espaco_caixa = 5 #definindo a variavel espaco_caixa (volume)
	caixas = 60 #definindo a variavel caixas (unidades de caixas)
	cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
	caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
	caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa
	print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")
	print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
	print ("Em cada caixa cabem", espaco_caixa, "cebolas")
	print ("Ainda temos,", caixas_vazias, "caixas vazias")
	print ("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")

	print("--------------N2.3--------------")
	cebolas = quantidade("\nHá quantas cebolas no total?\n") 
	cebolas_na_caixa =  quantidade("Há quantas cebolas dentro da caixa?\n")
	espaco_caixa = quantidade("Qual é o espaço da caixa?\n")
	caixas = quantidade("Há quantas caixas no total?\n")
	cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
	caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
	caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa
	print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")
	print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
	print ("Em cada caixa cabem", espaco_caixa, "cebolas")
	print ("Ainda temos,", caixas_vazias, "caixas vazias")
	print ("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")


n2()