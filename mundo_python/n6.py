"""6.1 Armazene os nomes de alguns de seus amigos em uma
lista chamada nomes. Imprima o nome de cada pessoa
acessando cada elemento da lista, um de cada vez.
6.2 Comece com a lista que você criou no exercício 6.1,
mas em vez de apenas imprimir o nome de cada pessoa,
imprima uma mensagem para ela. O texto de cada mensagem
deve ser o mesmo, mas cada mensagem deve conter o nome da
pessoa específica.
6.3 Pense no seu modo de transporte favorito, como uma
moto ou um carro, e faça uma lista que armazene modelos
do seu transporte favorito. Use sua lista para imprimir
uma série de mensagens sobre esses itens, como "Eu
gostaria de ter uma moto da Honda".
"""
 
def n6():
    print("-------EXERCICIO 6.1-------")
    amigos = ['jade', 'paula', 'lirah', 'rafa']
    print(amigos[0])
    print(amigos[1])
    print(amigos[2])
    print(amigos[3])
     
    print("-------EXERCICIO 6.2-------")
     
    for x in range(len(amigos)): 
        print("Oi", amigos[x], ", como vai?")

    print("-------EXERCICIO 6.3-------")
    transporte = ['metrô', 'carros elétricos de aluguel', 'patinetes elétricos de aluguel']
    for x in range(len(transporte)): 
        print("Eu gostaria que na minha cidade houvesse um bom sistema de", transporte[x])


n6()