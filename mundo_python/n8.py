"""8.1 Pense em pelo menos cinco lugares no mundo que você gostaria de visitar.

0 Armazene os locais em uma lista. Certifique­se de que a lista não esteja em ordem alfabética.
1 Imprima sua lista na ordem original. Não se preocupe em imprimir a lista item por item, basta imprimi­la como uma lista bruta do Python.
2 Use sorted() para imprimir sua lista em ordem alfabética sem modificar a lista atual.
3 Mostre que sua lista ainda está em sua ordem original, imprimindo­a.
4 Use sorted() para imprimir sua lista em ordem alfabética inversa sem alterar a ordem da lista original.
5 Mostre que sua lista ainda está em sua ordem original, imprimindo­a.
6 Use reverse() para alterar a ordem da sua lista. Imprima a lista para mostrar que a ordem foi alterada.
7 Use reverse() para alterar a ordem da sua lista novamente. Imprima a lista para mostrar que ela está de volta ao pedido original.
8 Use sort() para que a lista seja armazenada em ordem alfabética. Imprima a lista para mostrar que a ordem foi alterada.
8 Use sort() para que a lista seja armazenada em ordem alfabética inversa. Imprima a lista para mostrar que a ordem foi alterada.

8.2 Altere os códigos desenvolvidos para o convite de jantar (Notas de aula 7)
para que seja apresentada a quantidade de convidados existentes.

8.3 Todas as funções! Pense em algo que você gostaria armazenar em uma lista.
Por exemplo, você pode criar uma lista de montanhas, rios, países, cidades, idiomas ou
qualquer outra coisa que deseje. Escreva um programa que crie uma lista contendo esses
itens e, em seguida, use cada função apresentada nesta nota pelo menos"""

def n8():

    print("-------EXERCICIO 8.1-------")
    visita = ['chile', 'canada', 'argentina', 'espanha', 'egito', 'alemanha']
    print('1', visita)
    print('2', sorted(visita))
    print('3', visita)
    print('4', sorted(visita).reverse())
    print('5', visita)
    visita.reverse()
    print('6', visita)
    visita.reverse()
    print('7', visita)
    visita.sort()
    print('8', visita)
    visita.sort(reverse=True)
    print('9', visita)
    
    print("-------EXERCICIO 8.2-------")
    amigos = ['Vitor', 'Rafa', 'Chico']
    print('A lista de convidados possui', len(amigos), ' pessoas')
    
    print("-------EXERCICIO 8.3-------")
    lista = ['uva', 'kiwi', 'ameixa', 'pera', 'leite', 'queijo', 'pão']
    print('Lista de compras:', lista)
    print('Já tem queijo')
    del lista[5]
    print('Lista de compras:', lista)
    print('Vou comprar leite depois')
    lista_depois = lista.pop(4)
    print('Lista de compras:', lista)
    print('Comprar depois:', lista_depois)
    print('Por enquanto há', len(lista), 'itens na lista')
    print('Lembrei que preciso de mel')
    lista.insert(0, 'mel')
    print(lista)
    print('Agora há', len(lista), 'itens na lista')
    
n8()

