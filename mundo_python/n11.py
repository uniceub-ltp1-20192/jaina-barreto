"""11.1 Slices: Escolha algum dos programas que você fez sobre listas, e adicione algumas linhas ao final do programa para que ele faça o seguinte:

• Imprima a seguinte mensagem: “Os primeiros 3 itens da lista são: ”. E use um slice para imprimir os três primeiros itens da lista do programa escolhido.
• Imprima a seguinte mensagem: “Os últimos 3 itens da lista são: ”. E use um slice para imprimir os três últimos itens da lista do programa escolhido.
• Imprima a seguinte mensagem: “Os 3 itens no meio da lista são: ”. E use um slice para imprimir os três itens no meio da lista do programa escolhido.

11.2 Crie uma lista de sabores de pizza e armazene em pizza_hut. Faça uma cópia da lista de pizzas e armazene em pizza_hot_paraguai. E faça o que se pede:
• Adiciona uma pizza em pizza_hut.
• Adicione uma pizza diferente em pizza_hot_paraguai.
• Demonstre que a pizza hut original é diferente da cópia paraguaia!

11.3 Quero loops, mais loops! Todos os códigos nessa nota de aula imprimiram as listas sem o uso de loops. Escolha um código dessa nota e imprima todos 
os itens como um cardápio. Use um loop para cada lista."""

def n11():

    print("-------EXERCICIO 11.1-------")
    visita = ['chile', 'canadá', 'argentina', 'espanha', 'egito', 'alemanha']
    print('Lista inicial:', visita)
    print('Os três primeiros itens da lista são:', visita[0:3])
    print('Os três últimos itens da lista são:', visita[-3:])
    print('Os três itens do meio da lista são:', visita[1:4])

    print("-------EXERCICIO 11.2-------")
    pizza_hut = ['marguerita', 'portuguesa', 'alho']
    pizza_hot_paraguai = pizza_hut[:]
    pizza_hut.append('lombo')
    pizza_hot_paraguai.append('calabresa')

    print(pizza_hut)
    print(pizza_hot_paraguai)

    print("-------EXERCICIO 11.3-------")
    print('Países:')
    for paises in visita:
        print(paises.title())

    print('\nPizzas do Pizza Hut:')
    for sabores in pizza_hut:
        print(sabores.title())

    print('\nPizzas do Pizza Hot Paraguai:')
    for sabores in pizza_hut:
        print(sabores.title())

n11()
