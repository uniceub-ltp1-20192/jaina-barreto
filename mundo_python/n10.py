"""10.1 Use o loop for para imprimir os números de 1 a 20
10.2 Faça uma lista de de 1 a um milhão e depois imprima cada um dos valores da lista.
10.3 Faça uma lista de de 1 a um milhão e use as funções min() e max() para grantir que
sua lista realmente começa em 1 e termina em um milhão. Depois utilize a função sum()
para imprimir a soma dos valores da lista.
10.4 Use o terceiro parâmetro de função range para fazer uma lista com os númerosimpares
de 1 a 20. Imprima cada elemento da lista separadamente.
10.5 Faça uma lista com todos os números múltiplos de 3, no intervalo de 3 a 1000.
Imprima cada valor separadamente.
10.6 Um número elevado a terceira potência e chamado de cubo. Faça uma lista com todos os
cubos entre 1 e 100, imprima cada valor separadamente
10.7 Faça compreensão de lista para gerar a lista do exercício 10.6"""

def n10():

    print("-------EXERCICIO 10.1-------")
    for valor in range (1,21):
        print(valor)

    print("-------EXERCICIO 10.2-------")
    lista = [valor for valor in range(1,1000001)]
#    print(lista)
    print("-------EXERCICIO 10.3-------")
    print(min(lista))
    print(max(lista))
#    print(sum(lista))

    print("-------EXERCICIO 10.4-------")
    impares = [imp for imp in range(1, 21, 2)]
    print(impares)

    print("-------EXERCICIO 10.5-------")
    multi_tres = []
    for mt in range(3,1000):
        if mt % 3 == 0:
            multi_tres.append(mt)
    print(multi_tres)

    print("-------EXERCICIO 10.6-------")
    cubos = [cubo**3 for cubo in range(1,1000)]
    print(cubos)

n10()